<?php

use Illuminate\Database\Seeder;

class AdministratorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $administrator = new \App\User;
        $administrator->name = "admin";
        $administrator->email = "admin@job.test";
        $administrator->password = \Hash::make("jobtest");
        $administrator->save();
        $this->command->info("User Admin insert success");
    }
}
