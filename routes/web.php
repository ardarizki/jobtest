<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
Route::get('/', 'HomeController@index')->name('home');
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout')->name('logout');

Route::group(['middleware' => ['web', 'auth']], function()
{
    Route::get('/teacher/getdata', 'TeacherController@getdata');
    Route::get('/classroom/getdata', 'ClassroomController@getdata');
    Route::get('/student/getdata', 'StudentController@getdata');

    Route::resource('/teacher','TeacherController');
    Route::resource('/classroom','ClassroomController');
    Route::resource('/student','StudentController');
});