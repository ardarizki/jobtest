@extends('layouts.header')

@section('content')
<div class="row">
    <div class="col s12">
        <h5>Master - Class Room</h5><hr>
    </div>
    <div class="col s12">
        <button class="waves-effect orange accent-3 btn modal-trigger left" id="btn-new">New Classroom</button>
            <table id="table" class="display nowrap" style="width:100%">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Teacher</th>
                        <th  class="right-align">Action</th>
                    </tr>
                </thead>
            </table>
    </div>
</div>

<div id="new-classroom" class="modal modal-fixed-footer">
    <div class="modal-content">
        <h5>Add new classroom</h5><hr>
        <div class="row">
            <div class="input-field col s6">
                <label for="name">Name</label>
                <input placeholder="Write classroom's name..." id="name" type="text" class="validate">
            </div>
            <div class="input-field col s6">
                <select id="teacher_id">
                </select>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <a class="modal-close waves-effect waves-green btn-flat">Cancel</a>
        <button class="waves-effect waves-green btn" id="btn-save">Save</button>
    </div>
</div>

<div id="edit-classroom" class="modal modal-fixed-footer">
    <div class="modal-content">
        <h5>Edit classroom</h5><hr>
        <div class="row">
        <div class="input-field col s6">
                <label for="name">Name</label>
                <input placeholder="Write classroom's name..." id="up_name" type="text" class="validate">
            </div>
            <div class="input-field col s6">
                <select id="up_teacher_id">
                </select>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <a class="modal-close waves-effect waves-green btn-flat">Cancel</a>
        <button class="waves-effect waves-green btn" id="btn-update">Update</button>
    </div>
</div>

<div id="delate-classroom" class="modal">
    <div class="modal-content">
        <h5>Delete classroom</h5><hr>
        <div class="row">
            <div class="input-field col s12">
                <center>Delete classtoom id <b id="res-id"></b> ?</center>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <a class="modal-close waves-effect waves-green btn-flat">No</a>
        <button class="waves-effect red accent-3 btn" id="btn-delete">Yes</button>
    </div>
</div>
@endsection

@section('js')
<script>
var table;
var id_edit;
var id_delete;
$(document).ready(function() {
    loadTable()
    loadTeacher()
});
function loadTeacher(){
    $.ajax({
        url : "/teacher/getdata",
        type : "get",
        dataType: "json",
        success: function(data){
            var html = "<option value='' disabled selected>Choose a teacher</option>";
            for(var a=0; a<data.data.length; a++){
                html += "<option value='"+data.data[a].teacher_id+"'>"+data.data[a].teacher_name+"</option>"
            }
            $("#teacher_id").html(html)
            $('#teacher_id').formSelect();
            $("#up_teacher_id").html(html)
            $('#up_teacher_id').formSelect();
        }
    })
}
function loadTable(){
    table = $('#table').DataTable({
        ajax : {
            url : '/classroom/getdata',
            dataSrc : function(json){
                var return_data = new Array()
                $.each(json['data'], function(i, item){
                    return_data.push({
                        'id' : item['classroom_id'],
                        'name' : item['classroom_name'],
                        'teach' : item['teacher'].teacher_name,
                        'action' : '<button onclick="edit('+item['classroom_id']+')" class="waves-effect grey darken-3 btn-small right"><i class="material-icons">edit</i></button>'
                        +' <button onclick="fdelete('+item['classroom_id']+')" class="waves-effect red accent-3 btn-small right"><i class="material-icons">delete</i></button>',
                    })
                });
            return return_data
            }
        },
        columns : [
            {data: 'id'},
            {data: 'name'},
            {data: 'teach'},
            {data: 'action'},
        ],
        "bLengthChange": false,
        select: true,
        destroy: true,
        buttons: [
            'pdf',
        ]
    });
}
$("#btn-new").click(function(){
    $("#new-classroom").modal('open')
})
$("#btn-save").click(function(){
    $("#load-page").show()
    $.ajax({
        url: "/classroom",
        type: "post",
        dataType: "json",
        data : {
            name : $("#name").val(),
            teacher_id : $("#teacher_id").val(),
            _token : "{{csrf_token()}}",
        },
        success: function(data){
            $("#new-classroom").modal('close')
            M.toast({html: data.data})
            $("#load-page").hide()
            table.ajax.reload()
            $("#name").val("")
        },
        error: function(data){
            M.toast({html: data.data})
            $("#load-page").hide()
        }
    })
})
function edit(id){
    $("#load-page").show()
    $.ajax({
        url: "/classroom/"+id+"/edit",
        type: "get",
        dataType: "json",
        success: function(data){
            $("#load-page").hide()
            $("#edit-classroom").modal('open')
            $("#up_name").val(data.classroom_name)
            $("#up_teacher_id").val(data.teacher_id).attr('selected')
            id_edit = data.classroom_id
        }
    })
}
$("#btn-update").click(function(){
    $("#load-page").show()
    $.ajax({
        url: "/classroom/"+id_edit,
        type:"post",
        dataType: "json",
        data: {
            _method: "put",
            _token: "{{csrf_token()}}",
            name: $("#up_name").val(),
            teacher_id: $("#up_teacher_id").val(),
        },
        success: function(data){
            $("#load-page").hide()
            M.toast({html: data.data})
            $("#edit-classroom").modal('close')
            table.ajax.reload()
        },
        error: function(data){
            M.toast({html: data.data})
            $("#load-page").hide()
        }
    })
})
function fdelete(id){
    $("#delate-classroom").modal('open')
    $("#res-id").html(id);
    id_delete = id;
}
$("#btn-delete").click(function(){
    $("#load-page").show()
    $.ajax({
        url: "/classroom/"+id_delete,
        type: "post",
        dataType: "json",
        data: {
            _method: "delete",
            _token: "{{csrf_token()}}",
        },
        success: function(data){
            $("#load-page").hide()
            $("#delate-classroom").modal('close')
            M.toast({html: data.data})
            table.ajax.reload()
        },
        error: function(data){
            M.toast({html: data.data})
            $("#load-page").hide()
        }
    })
})
</script>
@endsection