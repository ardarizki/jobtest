@extends('layouts.header')

@section('content')
<div class="row">
    <div class="col s12">
        <h5>Master - Teacher</h5><hr>
    </div>
    <div class="col s12" id="div-content">
        <button class="waves-effect orange accent-3 btn modal-trigger left" id="btn-new">New Teacher</button>
            <table id="table" class="display nowrap" style="width:100%">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th  class="right-align">Action</th>
                    </tr>
                </thead>
            </table>
    </div>
</div>

<div id="new-teacher" class="modal">
    <div class="modal-content">
        <h5>Add new teacher</h5><hr>
        <div class="row">
            <div class="input-field col s12">
                <label for="name">Name</label>
                <input placeholder="Write teacher's name..." id="name" type="text" class="validate">
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <a class="modal-close waves-effect waves-green btn-flat">Cancel</a>
        <button class="waves-effect waves-green btn" id="btn-save">Save</button>
    </div>
</div>

<div id="edit-teacher" class="modal">
    <div class="modal-content">
        <h5>Edit teacher</h5><hr>
        <div class="row">
            <div class="input-field col s12">
                <label>Name</label>
                <input id="up_name" placeholder="Write teacher's name..." type="text" class="validate">
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <a class="modal-close waves-effect waves-green btn-flat">Cancel</a>
        <button class="waves-effect waves-green btn" id="btn-update">Update</button>
    </div>
</div>

<div id="delate-teacher" class="modal">
    <div class="modal-content">
        <h5>Delete teacher</h5><hr>
        <div class="row">
            <div class="input-field col s12">
                <center>Delete teacher id <b id="res-id"></b> ?</center>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <a class="modal-close waves-effect waves-green btn-flat">No</a>
        <button class="waves-effect red accent-3 btn" id="btn-delete">Yes</button>
    </div>
</div>
@endsection

@section('js')
<script>
var table;
var id_edit;
var id_delete;
$(document).ready(function() {
    loadTable()
});
function loadTable(){
    table = $('#table').DataTable({
        ajax : {
            url : '/teacher/getdata',
            dataSrc : function(json){
                var return_data = new Array()
                $.each(json['data'], function(i, item){
                    return_data.push({
                        'id' : item['teacher_id'],
                        'name' : item['teacher_name'],
                        'action' : '<button onclick="edit('+item['teacher_id']+')" class="waves-effect grey darken-3 btn-small right"><i class="material-icons">edit</i></button>'
                        +' <button onclick="fdelete('+item['teacher_id']+')" class="waves-effect red accent-3 btn-small right"><i class="material-icons">delete</i></button>',
                    })
                });
            return return_data
            }
        },
        columns : [
            {data: 'id'},
            {data: 'name'},
            {data: 'action'},
        ],
        "bLengthChange": false,
        select: true,
        destroy: true,
        buttons: [
            'pdf',
        ]
    });
}

$("#btn-new").click(function(){
    $("#new-teacher").modal('open')
})
$("#btn-save").click(function(){
    $("#load-page").show()
    $.ajax({
        url: "/teacher",
        type: "post",
        dataType: "json",
        data : {
            name : $("#name").val(),
            _token : "{{csrf_token()}}",
        },
        success: function(data){
            $("#new-teacher").modal('close')
            M.toast({html: data.data})
            $("#load-page").hide()
            table.ajax.reload()
            $("#name").val("")
        },
        error: function(data){
            M.toast({html: data.data})
            $("#load-page").hide()
        }
    })
})
function edit(id){
    $("#load-page").show()
    $.ajax({
        url: "/teacher/"+id+"/edit",
        type: "get",
        dataType: "json",
        success: function(data){
            $("#load-page").hide()
            $("#edit-teacher").modal('open')
            $("#up_name").val(data.teacher_name)
            id_edit = data.teacher_id
        }
    })
}
$("#btn-update").click(function(){
    $("#load-page").show()
    $.ajax({
        url: "/teacher/"+id_edit,
        type:"post",
        dataType: "json",
        data: {
            _method: "put",
            _token: "{{csrf_token()}}",
            name: $("#up_name").val(),
        },
        success: function(data){
            $("#load-page").hide()
            M.toast({html: data.data})
            $("#edit-teacher").modal('close')
            table.ajax.reload()
        },
        error: function(data){
            M.toast({html: data.data})
            $("#load-page").hide()
        }
    })
})

function fdelete(id){
    $("#delate-teacher").modal('open')
    $("#res-id").html(id);
    id_delete = id;
}

$("#btn-delete").click(function(){
    $("#load-page").show()
    $.ajax({
        url: "/teacher/"+id_delete,
        type: "post",
        dataType: "json",
        data: {
            _method: "delete",
            _token: "{{csrf_token()}}",
        },
        success: function(data){
            $("#load-page").hide()
            $("#delate-teacher").modal('close')
            M.toast({html: data.data})
            table.ajax.reload()
        },
        error: function(data){
            M.toast({html: data.data})
            $("#load-page").hide()
        }
    })
})
</script>
@endsection