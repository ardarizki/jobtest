@extends('layouts.header')

@section('content')
<div class="row">
    <div class="col s12">
        <h5>Master - Student</h5><hr>
    </div>
    <div class="col s12">
        <button class="waves-effect orange accent-3 btn modal-trigger left" id="btn-new">New Student</button>
            <table id="table" class="display nowrap" style="width:100%">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Classroom</th>
                        <th  class="right-align">Action</th>
                    </tr>
                </thead>
            </table>
    </div>
</div>

<div id="new-student" class="modal modal-fixed-footer">
    <div class="modal-content">
        <h5>Add new student</h5><hr>
        <div class="row">
            <div class="input-field col s6">
                <label for="name">Name</label>
                <input placeholder="Write student's name..." id="name" type="text" class="validate">
            </div>
            <div class="input-field col s6">
                <select id="classroom_id">
                </select>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <a class="modal-close waves-effect waves-green btn-flat">Cancel</a>
        <button class="waves-effect waves-green btn" id="btn-save">Save</button>
    </div>
</div>

<div id="edit-student" class="modal modal-fixed-footer">
    <div class="modal-content">
        <h5>Edit student</h5><hr>
        <div class="row">
        <div class="input-field col s6">
                <label for="name">Name</label>
                <input placeholder="Write student's name..." id="up_name" type="text" class="validate">
            </div>
            <div class="input-field col s6">
                <select id="up_classroom_id">
                </select>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <a class="modal-close waves-effect waves-green btn-flat">Cancel</a>
        <button class="waves-effect waves-green btn" id="btn-update">Update</button>
    </div>
</div>

<div id="delate-student" class="modal">
    <div class="modal-content">
        <h5>Delete student</h5><hr>
        <div class="row">
            <div class="input-field col s12">
                <center>Delete student id <b id="res-id"></b> ?</center>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <a class="modal-close waves-effect waves-green btn-flat">No</a>
        <button class="waves-effect red accent-3 btn" id="btn-delete">Yes</button>
    </div>
</div>
@endsection

@section('js')
<script>
var table;
var id_edit;
var id_delete;
$(document).ready(function() {
    loadTable()
    loadClassroom()
});
function loadClassroom(){
    $.ajax({
        url : "/classroom/getdata",
        type : "get",
        dataType: "json",
        success: function(data){
            var html = "<option value='' disabled selected>Choose a classroom</option>";
            for(var a=0; a<data.data.length; a++){
                html += "<option value='"+data.data[a].classroom_id+"'>"+data.data[a].classroom_name+" - "+data.data[a].teacher.teacher_name+"</option>"
            }
            $("#classroom_id").html(html)
            $('#classroom_id').formSelect();
            $("#up_classroom_id").html(html)
            $('#up_classroom_id').formSelect();
        }
    })
}
function loadTable(){
    table = $('#table').DataTable({
        ajax : {
            url : '/student/getdata',
            dataSrc : function(json){
                var return_data = new Array()
                $.each(json['data'], function(i, item){
                    return_data.push({
                        'id' : item['id'],
                        'name' : item['student_name'],
                        'class' : item['classroom'].classroom_name,
                        'action' : '<button onclick="edit('+item['id']+')" class="waves-effect grey darken-3 btn-small right"><i class="material-icons">edit</i></button>'
                        +' <button onclick="fdelete('+item['id']+')" class="waves-effect red accent-3 btn-small right"><i class="material-icons">delete</i></button>',
                    })
                });
            return return_data
            }
        },
        columns : [
            {data: 'id'},
            {data: 'name'},
            {data: 'class'},
            {data: 'action'},
        ],
        "bLengthChange": false,
        select: true,
        destroy: true,
        buttons: [
            'pdf',
        ]
    });
}
$("#btn-new").click(function(){
    $("#new-student").modal('open')
})
$("#btn-save").click(function(){
    $("#load-page").show()
    $.ajax({
        url: "/student",
        type: "post",
        dataType: "json",
        data : {
            name : $("#name").val(),
            classroom_id : $("#classroom_id").val(),
            _token : "{{csrf_token()}}",
        },
        success: function(data){
            $("#new-student").modal('close')
            M.toast({html: data.data})
            $("#load-page").hide()
            table.ajax.reload()
            $("#name").val("")
        },
        error: function(data){
            M.toast({html: data.data})
            $("#load-page").hide()
        }
    })
})
function edit(id){
    $("#load-page").show()
    $.ajax({
        url: "/student/"+id+"/edit",
        type: "get",
        dataType: "json",
        success: function(data){
            $("#load-page").hide()
            $("#edit-student").modal('open')
            $("#up_name").val(data.student_name)
            $("#up_classroom_id").val(data.classroom_id).attr('selected')
            id_edit = data.id
        }
    })
}
$("#btn-update").click(function(){
    $("#load-page").show()
    $.ajax({
        url: "/student/"+id_edit,
        type:"post",
        dataType: "json",
        data: {
            _method: "put",
            _token: "{{csrf_token()}}",
            name: $("#up_name").val(),
            classroom_id: $("#up_classroom_id").val(),
        },
        success: function(data){
            $("#load-page").hide()
            M.toast({html: data.data})
            $("#edit-student").modal('close')
            table.ajax.reload()
        },
        error: function(data){
            M.toast({html: data.data})
            $("#load-page").hide()
        }
    })
})
function fdelete(id){
    $("#delate-student").modal('open')
    $("#res-id").html(id);
    id_delete = id;
}
$("#btn-delete").click(function(){
    $("#load-page").show()
    $.ajax({
        url: "/student/"+id_delete,
        type: "post",
        dataType: "json",
        data: {
            _method: "delete",
            _token: "{{csrf_token()}}",
        },
        success: function(data){
            $("#load-page").hide()
            $("#delate-student").modal('close')
            M.toast({html: data.data})
            table.ajax.reload()
        },
        error: function(data){
            M.toast({html: data.data})
            $("#load-page").hide()
        }
    })
})
</script>
@endsection