@extends('layouts.header')

@section('content')
<div class="row">
    <div class="col s12">
        <h5>Home</h5><hr>
    </div>
    <div class="col s12">
        <table id="table" class="display nowrap" style="width:100%">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Classroom</th>
                    <th>Teacher</th>
                    <th>Student</th>
                </tr>
            </thead>
            <tbody>
            <?php $i = 1;?>
            @foreach($data as $d)
                <tr>
                    <td>{{$i++}}</td>
                    <td>{{$d->classroom_name}}</td>
                    <td>{{$d->teacher->teacher_name}}</td>
                    <td>
                    @foreach($d->student as $s)
                        {{$s->student_name}},
                    @endforeach
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection()
@section('js')
<script>
$(document).ready(function(){
    $('#table').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'pdf',
        ]
    })
});
</script>
@endsection