<!DOCTYPE HTML>
<header>
    <title>Class Management</title>
    <link rel="stylesheet" href="../assets/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="../assets/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="../assets/icon.css">
    <link rel="stylesheet" href="../assets/css/materialize.min.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" media="screen,projection"/>
</header>
<body>
<div id="load-page" style="position: absolute; z-index: 9999; background-color: rgba(0,0,0,0.5); width:100%; height:100%;">
    <div class="progress" style="width: 33%; top:50%; left:33%;">
      <div class="indeterminate"></div>
    </div>
</div>

<div class="navbar-fixed">
    <ul id="drop_master" class="dropdown-content">
    <li><a href="/teacher">Teacher</a></li>
    <li><a href="/classroom">Class Room</a></li>
    <li><a href="/student">Student</a></li>
    </ul>
    <nav class="teal accent-4">
        <div class="nav-wrapper">
            <a href="/" class="brand-logo">Class Management</a>
            <ul id="nav-mobile" class="right hide-on-med-and-down">
                <li><a class="dropdown-trigger" href="javascript:void(0)" data-target="drop_master">Master<i class="material-icons right">arrow_drop_down</i></a></li>
                <li><a href="{{ route('logout') }}">Logout</a></li>
            </ul>
        </div>
    </nav>
</div>

<div class="container"><br>
    @yield('content')
</div>
</body>
<script type="text/javascript" src="../assets/customjs/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="../assets/customjs/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="../assets/customjs/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="../assets/customjs/buttons.html5.min.js"></script>
<script type="text/javascript" src="../assets/customjs/pdfmake.min.js"></script>
<script type="text/javascript" src="../assets/customjs/vfs_fonts.js"></script>
<script type="text/javascript" src="../assets/js/bin/materialize.min.js"></script>
<script>
$(document).ready(function(){
    $(".dropdown-trigger").dropdown();
    $(".modal").modal();
    $('select').formSelect();
    $("#load-page").hide();
});
</script>
@yield('js')