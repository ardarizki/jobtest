<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $table = 'student';
    protected $primaryKey = 'id';
    protected $fillable = ["student_name", "classroom_id"];
    protected $with = ['classroom'];

    public function classroom()
    {
        return $this->belongsTo('App\Classroom', 'classroom_id');
    }
}
