<?php

namespace App\Http\Controllers;

use App\Classroom;
use Illuminate\Http\Request;

class ClassroomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('master.classroom');
    }

    public function getdata()
    {
        $classroom = Classroom::all();
        
        return response()->json(['data' => $classroom]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'teacher_id' => 'required',
        ]);

        $classroom = new Classroom;
        $classroom->classroom_name = $request->name;
        $classroom->teacher_id = $request->teacher_id;

        $insert = $classroom->save();

        if($insert)
        {
            return response()->json(['data' => 'Data saved.']);
        }else{
            return response()->json(['data' => 'Failed.']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $classroom = Classroom::find($id);

        return response()->json($classroom);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);

        $classroom = Classroom::find($id);
        $classroom->classroom_name = $request->name;
        $classroom->teacher_id = $request->teacher_id;

        $update = $classroom->save();

        if($update)
        {
            return response()->json(['data' => 'Data updated.']);
        }else{
            return response()->json(['data' => 'Failed.']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $classroom = Classroom::find($id);

        $delete = $classroom->delete();

        if($delete)
        {
            return response()->json(['data' => 'Data deleted.']);
        }else{
            return response()->json(['data' => 'Failed.']);
        }
    }
}
