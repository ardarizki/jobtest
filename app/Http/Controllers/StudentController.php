<?php

namespace App\Http\Controllers;

use App\Student;
use Illuminate\Http\Request;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('master.student');
    }

    public function getdata()
    {
        $student = Student::all();
        
        return response()->json(['data' => $student]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'classroom_id' => 'required',
        ]);

        $student = new Student;
        $student->student_name = $request->name;
        $student->classroom_id = $request->classroom_id;

        $insert = $student->save();

        if($insert)
        {
            return response()->json(['data' => 'Data saved.']);
        }else{
            return response()->json(['data' => 'Failed.']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $student = Student::find($id);

        return response()->json($student);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'classroom_id' => 'required',
        ]);

        $student = Student::find($id);
        $student->student_name = $request->name;
        $student->classroom_id = $request->classroom_id;

        $update = $student->save();

        if($update)
        {
            return response()->json(['data' => 'Data updated.']);
        }else{
            return response()->json(['data' => 'Failed.']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $student = Student::find($id);

        $delete = $student->delete();

        if($delete)
        {
            return response()->json(['data' => 'Data deleted.']);
        }else{
            return response()->json(['data' => 'Failed.']);
        }
    }
}
