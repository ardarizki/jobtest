<?php

namespace App\Http\Controllers;

use App\Teacher;
use Illuminate\Http\Request;

class TeacherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('master.teacher');
    }

    public function getdata()
    {
        $teacher = Teacher::all();
        
        return response()->json(['data' => $teacher]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);

        $teacher = new Teacher;
        $teacher->teacher_name = $request->name;

        $insert = $teacher->save();

        if($insert)
        {
            return response()->json(['data' => 'Data saved.']);
        }else{
            return response()->json(['data' => 'Failed.']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $teacher = Teacher::find($id);

        return response()->json($teacher);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   
        $this->validate($request, [
            'name' => 'required',
        ]);

        $teacher = Teacher::find($id);
        $teacher->teacher_name = $request->name;

        $update = $teacher->save();

        if($update)
        {
            return response()->json(['data' => 'Data updated.']);
        }else{
            return response()->json(['data' => 'Failed.']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $teacher = Teacher::find($id);
        $delete = $teacher->delete();

        if($delete)
        {
            return response()->json(['data' => 'Data deleted.']);
        }else{
            return response()->json(['data' => 'Failed.']);
        }
    }
}
