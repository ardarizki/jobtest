<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Classroom extends Model
{
    protected $table = 'classroom';
    protected $primaryKey = 'classroom_id';
    protected $fillable = ["classroom_name", "teacher_id"];
    protected $with = ['teacher'];

    public function teacher()
    {
        return $this->belongsTo('App\Teacher', 'teacher_id');
    }

    public function student()
    {
        return $this->hasMany('App\Student', 'classroom_id');
    }
}
