<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
    protected $table = 'teacher';
    protected $primaryKey = 'teacher_id';
    protected $fillable = ["teacher_name"];

    public function classrooms()
    {
        return $this->hasMany('App\Classroom', 'teacher_id');
    }
}
